usage: SearchBook.py [-h] [-t {a,p,i,k,p}] word

湖北大学图书馆图书查找脚本

positional arguments:
  word            the keyword of you searched

optional arguments:
  -h, --help        show this help message and exit
  -t {a,p,i,k,p}    search type(
                      t:title(default)
                      a:author
                      p:publisher
                      k:keyword
                      i:isbn)
