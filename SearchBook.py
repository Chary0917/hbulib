#!/usr/bin/env python
# -*-coding:utf-8-*-

'''
usage: SearchBook.py [-h] [-t {a,p,i,k,p}] word

湖北大学图书馆图书查找脚本

positional arguments:
  word            the keyword of you searched

optional arguments:
  -h, --help        show this help message and exit
  -t {a,p,i,k,p}    search type(
                      t:title(default)
                      a:author
                      p:publisher
                      k:keyword
                      i:isbn)
'''


import sys
import argparse
from urllib import request
from urllib import parse
from html.parser import HTMLParser


__author__ = 'chary'
__version__= 0.1


search_url = 'http://59.68.64.61:8080/opac/openlink.php?'
search_type = {'a':'author','t':'title',\
               'k':'keyword','i':'isbn','p':'publisher'}



class MyHtmlPraser(HTMLParser):
    '自定义的HTMLPraser类'
    _recode_flag =False                   #总Flag
    _recode_book_type_flag =False         #图书类型
    _recode_pos_flag = False              #索引号标志
    _recode_title_flag =False             #标题
    _recode_publisher_flag = False        #出版社
    _recode_have_books_flag = False       #总图书
    _recode_can_books_flag =False         #可借图书
    _recode_name_book_flag =False         #作者

    _book_title=''
    _book_name =''
    _pos_=''
    _book_type_ =''
    _publisher_=''
    _list_book =[]

    _book_num =[]

    def handle_charref(self,name):
        if  self._recode_flag and self._recode_title_flag:
            num = int('0'+ name,base = 16)
            self._book_title += chr(num)
            return

        if  self._recode_flag and self._recode_pos_flag:
            num = int('0'+ name,base = 16)
            self._pos_ += chr(num)
            return

        if self._recode_flag and self._recode_name_book_flag:
            num = int('0'+ name,base = 16)
            self._book_name += chr(num)
            return

        if self._recode_flag and self._recode_publisher_flag:
            num = int('0'+ name,base = 16)
            self._publisher_ += chr(num)
            return

    def handle_starttag(self, tag, attrs):
        if tag == 'div' and attrs == [('class','list_books'),('id','list_books')]:
            self._recode_flag = True
            return

        if self._recode_flag and tag == 'span' and attrs == [('class','doc_type_class')]:
            self._recode_book_type_flag = True
            return

        if self._recode_flag and tag == 'a':
            self._recode_title_flag = True
            return

    def handle_endtag(self, tag):
        if tag == 'div' and self._recode_flag:
            self._recode_flag = False
            result = (self._book_title,self._pos_,self._book_name,self._book_num,self._publisher_)
            formatprint(result)
            self._book_num.clear()
            self._book_title=''
            self._book_name =''
            self._pos_=''
            self._book_type_ =''
            self._publisher_=''
            return

        if self._recode_flag and tag == 'a':
            self._recode_title_flag = False
            self._recode_pos_flag = True
            return

        if self._recode_flag and tag == 'h3':
#            self._recode_book_type_flag = False
            self._recode_pos_flag = False
            return

        if self._recode_flag and self._recode_book_type_flag and tag == 'span':
            self._recode_book_type_flag =False
            return

        if self._recode_flag and not self._recode_book_type_flag and tag == 'span':
            self._recode_name_book_flag = True
            return

        if self._recode_flag and self._recode_publisher_flag and tag == 'p':
            self._recode_publisher_flag =  False
            return

    def handle_startendtag(self, tag, attrs):
        if self._recode_flag and self._recode_name_book_flag and tag == 'br':
            self._recode_name_book_flag = False
            self._recode_publisher_flag = True
            return

    def handle_data(self, data):
        if self._recode_flag and self._recode_book_type_flag:
            self._book_type_ = data
            return

        if self._recode_flag and self._recode_title_flag:
            self._book_title += data
            return

        if self._recode_flag and data == '馆藏复本：':
            self._recode_have_books_flag = True
            return

        if  self._recode_flag and data == '可借复本：':
            self._recode_have_books_flag = False
            self._recode_can_books_flag = True
            return

        if self._recode_flag and self._recode_have_books_flag:
            self._book_num.append(data)
            return

        if self._recode_flag and self._recode_can_books_flag:
            self._book_num.append(data)
            self._recode_can_books_flag = False
            return


def formatprint(data):
    print(data[0],end='')
    if data[1] == '':
        print('  索引号:\033[1;31;0m无法找到索引号\033[0m',end='')
    else:
        print('  索引号:\033[1;31;0m',data[1],'\033[0m' ,end='')
    print('  作者:',data[2],end='')
    print('  馆藏图书:',data[3][0],end='')
    print('  可借图书:',data[3][-1],end ='')
    print('  出版社:',data[4])



def searchbook(type,word):
    openurl = search_url + 'strSearchType='+ type +'&strText='+parse.quote(word)
    response = request.urlopen(openurl)
    data = response.read()
    parser =MyHtmlPraser()
    parser.feed(data.decode())



def main():
    parse = argparse.ArgumentParser(description='湖北大学图书馆图书查找脚本')
    parse.add_argument('-t', choices=['a', 'p', 'i','k','p'],help="search type(\
                        t:title(default)\
                        a:author\
                        p:publisher\
                        k:keyword\
                        i:isbn)",default = 't')
    parse.add_argument('word',help = 'the book of you searched')
    args = parse.parse_args()
    searchbook(search_type[args.t], args.word)




if __name__ == '__main__':
    main()
